package com.swiftwininc.test;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
   @RequestMapping(value= {"/","hello"})
   public String printHelloWorld(ModelMap model) {
      model.addAttribute("message", "Hello Spring MVC Framework!");
      return "hello";
   }
   
   @RequestMapping("/index")
   public String printHello(ModelMap model) {
      model.addAttribute("message", "Hello Spring from index");
      return "index";
   }
}